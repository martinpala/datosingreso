---
output:
  pdf_document: default
  html_document: default
---
# Set de datos de ingreso 2016

> Preparado por Martín Paladino para el curso Métodos Cuantitativos 2020 del Instituto Mora
> 
> Los datos se reúnen a fines pedagógicos. Favor de revisar cuidadósamente el script carga.R que produce estos datos antes de usarlos para otros fines
>
> Se permite la redistribución libre del archivo ingresos2016.csv *siempre que esté acompañado de los archivos leame.md y carga.R*

# Propósito

Dado que la ENIGH es una encuesta muy amplia, que reúne datos de diferentes temas medidos diferentes unidades de análisis el INEGI distribuye los datos en archivos separados. El procedimiento para reunir esos archivos es simple y está bien documentado, sin embargo para facilitar el uso pedagógico de estos datos se presenta un archivo con 11 variables seleccionadas y recodificadas. Los datos corresponden a las bases de ingreso y población, no se utilizan datos de los hogares. 

# Variables incluídas

Variable       | Defininción       | Recodificada     | Origen
---------------|-------------------|----------|------------
ingreso_trim   |Logaritmo natural de Ingreso trimiestral  | No | ingreso.sav
sexo           |Sexo de respondente | No | población.sav
edad           |Edad en años cumplidos | No | población.sav
nivelaprob  |Máximo nivel educativo aprobado | Sí | población.sav
edo_conyug  | Estado conyugal | No | población.sav
redsoc_3 | El nivel de dificultad o facilidad con que las personas podrían conseguir ayuda para conseguir un trabajo | No | poblacion.sav
segsoc | Contribuciones a la seguridad social | No | poblacion.sav
trabajo_mp | Es la situación que distingue a la población de 12 o más años de acuerdo con el desempeño o no de una actividad económica en el periodo de referencia, que permite clasificarla como parte de la Población Económicamente Activa (PEA)
o como parte de la Población No Económicamente Activa (PNEA) | No | poblacion.sav
hor_1 | El tiempo, en horas, que las personas dedicaron a trabajar en la semana pasada | Sí (se imputa 0 cuando PNEA) | poblacion.sav
lengua | Distingue hablantes exclusivos de lengua indígena, español y bilingües | Sí | poblacion.sav
disc | Indica si algún integrante del hogar (NO EL RESPONDENTE) tiene alguna discapacidad | Sí | poblacion.sav

# Origen de los datos

El archivo ingreso2016.csv fue elaborado a partir de las bases de datos de la Encuesta Nacional Ingreso Gasto del INEGI. La documentación es esos datos está disponible en https://www.inegi.org.mx/programas/enigh/nc/2016/ , así como los datos abiertos

# Formato

Los datos se presentan en formato .csv. Dado que en ese formato se pierden atributos como el orden de factores y las etiquetas también se incluye una versión en formato de SPSS.

# Reproducibilidad

Esta base de datos se puede generar con R utilizando el script carga.R. Este script lee los datos y produce el objeto `set_final`. Es necesario descargar y descomprimir en el proyecto de trabajo de R los siguientes archivos:

https://www.inegi.org.mx/contenidos/programas/enigh/nc/2016/microdatos/enigh2016_ns_ingresos_sav.zip

https://www.inegi.org.mx/contenidos/programas/enigh/nc/2016/microdatos/enigh2016_ns_poblacion_sav.zip


Las librerías necesarias se documentan en el script el proceso de recodificación se consideran autodocumentados por procedimiento en el script.




